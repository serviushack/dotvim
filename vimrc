filetype plugin indent on

syntax on  " Turn on that syntax highlighting
syntax sync fromstart

set listchars=tab:→\ ,space:·,eol:↵
if has('gui_running')
  set listchars=tab:→\ ,space:·,eol:$
endif

set tabstop=2 " Replace tab with 2 spaces
set softtabstop=2 " Replace tab with 4 spaces
set shiftwidth=2 " This is kind of like tabstop, except when you are in insert  mode you hit <C-D> or <C-T>
set expandtab " Replace tab with spaces
set incsearch "find the next match as we type the search
set ruler

set background=dark
if has('gui_running')
  set background=light
endif

set nocp
set hidden " This allows you to leave a buffer with out saving you will be promted to save upon quit
set showcmd "show incomplete cmds down the bottom

set scrolloff=3
set showmatch " Highlight matching (){}[]


" unicode/encoding settings
set encoding=utf8

let g:airline_powerline_fonts=1

let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'
